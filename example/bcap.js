const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const config = require('config');
const Log = require('log');
const log = new Log('debug');
const mysql = require('mysql2');
const Q = require('q');
const INFO = require('./downloadInfo.json');
const INFO_SCIF = require('./downloadInfoSCIF.json');
const INFO_PRO = require('./downloadInfoPRO.json');
const bcrypt = require('bcrypt');
const randomstring = require("randomstring");
// const fs = require('fs');
const exec = require('child_process').exec;
const os = require('os');

require('events').EventEmitter.prototype._maxListeners = 100;

let INFO_NO_URLS = [];
let INFO_NO_ACCESS = [];

var setVersionFromFile = function(os, build, version, index) {
    exec('ls -tr files/' + os + '/' + build + '/ | tail -n 1', function (error, stdout, stderr) {
        var versionNum = version;
        var extension = '.zip';

        if (stdout) {
            var filename = stdout.split('\n')[0];
            var rightHalf = filename.split('-')[1];
            var versionBits = rightHalf.split('.');
            var versionNum = versionBits[0] + '.' + versionBits[1] + '.' + versionBits[2];
        }

        switch(build) {
            case 'pro':
              INFO_NO_URLS_PRO[index].version = versionNum;
              INFO_NO_URLS_PRO[index].instructions = INFO_NO_URLS_PRO[index].instructions.replace('{filename}', filename);
              INFO_PRO[index].version = versionNum;
              INFO_PRO[index].localfile = filename;
              INFO_PRO[index].build = build;
              break;
            case 'scif':
              INFO_NO_URLS_SCIF[index].version = versionNum;
              INFO_NO_URLS_SCIF[index].instructions = INFO_NO_URLS_PRO[index].instructions.replace('{filename}', filename);
              INFO_SCIF[index].version = versionNum;
              INFO_SCIF[index].localfile = filename;
              INFO_SCIF[index].build = build;
              break;
            case 'beta':
              INFO_NO_URLS[index].version = versionNum;
              INFO_NO_URLS[index].instructions = INFO_NO_URLS_PRO[index].instructions.replace('{filename}', filename);
              INFO[index].version = versionNum;
              INFO[index].localfile = filename;
              INFO[index].build = build;
              break;
        }
    });
}

for (var key in INFO) {
    if (INFO[key].hidden != true) {
        INFO_NO_URLS.push({
            os: INFO[key].os,
            friendly_name: INFO[key].friendly_name,
            pronoun: INFO[key].pronoun,
            postfix: INFO[key].postfix,
            description: INFO[key].description,
            instructions: INFO[key].instructions,
            disabled: INFO[key].disabled,
            version: setVersionFromFile(INFO[key].os, 'beta', INFO[key].version, key)
        });
        INFO_NO_ACCESS.push({
            os: INFO[key].os,
            friendly_name: INFO[key].friendly_name,
            pronoun: INFO[key].pronoun,
            postfix: INFO[key].postfix,
            description: INFO[key].description
        });
    }
}

let INFO_NO_URLS_SCIF = [];
let INFO_NO_ACCESS_SCIF = [];

for (var key in INFO_SCIF) {
    if (INFO_SCIF[key].hidden != true) {
        INFO_NO_URLS_SCIF.push({
            os: INFO_SCIF[key].os,
            friendly_name: INFO_SCIF[key].friendly_name,
            pronoun: INFO_SCIF[key].pronoun,
            postfix: INFO_SCIF[key].postfix,
            description: INFO_SCIF[key].description,
            instructions: INFO_SCIF[key].instructions,
            disabled: INFO_SCIF[key].disabled,
            version: setVersionFromFile(INFO_SCIF[key].os, 'scif', INFO_SCIF[key].version, key)
        });
        INFO_NO_ACCESS_SCIF.push({
            os: INFO_SCIF[key].os,
            friendly_name: INFO_SCIF[key].friendly_name,
            pronoun: INFO_SCIF[key].pronoun,
            postfix: INFO_SCIF[key].postfix,
            description: INFO_SCIF[key].description
        });
    }
}

let INFO_NO_URLS_PRO = [];
let INFO_NO_ACCESS_PRO = [];

for (var key in INFO_PRO) {
    if (INFO_PRO[key].hidden != true) {
        INFO_NO_URLS_PRO.push({
            os: INFO_PRO[key].os,
            friendly_name: INFO_PRO[key].friendly_name,
            pronoun: INFO_PRO[key].pronoun,
            postfix: INFO_PRO[key].postfix,
            description: INFO_PRO[key].description,
            instructions: INFO_PRO[key].instructions,
            disabled: INFO_PRO[key].disabled,
            version: setVersionFromFile(INFO_PRO[key].os, 'pro', INFO_PRO[key].version, key)
        });
        INFO_NO_ACCESS_PRO.push({
            os: INFO_PRO[key].os,
            friendly_name: INFO_PRO[key].friendly_name,
            pronoun: INFO_PRO[key].pronoun,
            postfix: INFO_PRO[key].postfix,
            description: INFO_PRO[key].description
        });
    }
}

log.info('Starting Beta Download Service on port: ' + config.app.port);

var pool = mysql.createPool(config.mysql);

app.use(express.static('frontend'));

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

var router = express.Router();

/**
 * route - / - base route for healthcheck
 **/
router.get('/', function(req, res) {
    res.json({
        message: 'OK'
    });
});

/**
 * GET - /multiVerify/:build/:code - validate code and return download data
 * @param - code - code to be verified
 **/
router.route('/multiVerify/:build/:code')
    .get(function(req, res) {
        let appType = "beta";
        let nourl = INFO_NO_URLS;
        let noaccess = INFO_NO_ACCESS;

        switch (req.params.build) {
            case 'beta':
                appType = "(type is NULL OR type = 'beta')";
                nourl = INFO_NO_URLS;
                noaccess = INFO_NO_ACCESS;
                break;
            case 'scif':
                appType = "type = 'scif'";
                nourl = INFO_NO_URLS_SCIF;
                noaccess = INFO_NO_ACCESS_SCIF;
                break;
            case 'pro':
                appType = "type = 'pro'";
                nourl = INFO_NO_URLS_PRO;
                noaccess = INFO_NO_ACCESS_PRO;
                break;
            default:
                return res.status(400).end();
        }

        let query = 'SELECT * from download_codes WHERE code = ' + mysql.escape(req.params.code) + ' AND ' + appType;

        pool.getConnection(function(err, connection) {
            if (err) {
                connection.release();
                log.error('database error: ' + err);
                res.status(500);
                return;
            }
            connection.query(query, function(err, rows) {
                connection.release();
                if (!err) {
                    if (rows.length != 0) {
                        if (rows[0].max_count > rows[0].count) {
                            res.json(nourl);
                        } else {
                            res.status(403);
                            res.json(noaccess);
                        }
                    } else {
                        res.status(401);
                        res.json(noaccess);
                    }
                    return;
                }
            });
            connection.on('error', function(err) {
                log.error('database error: ' + err);
                res.status(500);
                return;
            });
        });
    })

/**
 * GET - /getFile/:code/:os - download actual file if available locally
 * @param - code - code to be incremented
 * @param - os - os download being requested
 **/
router.route('/getFile/:build/:code/:os')
    .get(function(req, res) {
        let appType = "beta";
        let info = INFO;

        switch (req.params.build) {
            case 'beta':
                appType = "(type is NULL OR type = 'beta')";
                info = INFO;
                break;
            case 'scif':
                appType = "type = 'scif'";
                info = INFO_SCIF;
                break;
            case 'pro':
                appType = "type = 'pro'";
                info = INFO_PRO;
                break;
            default:
                return res.status(400).end();
        }

        let query = 'UPDATE download_codes SET count = count + 1 WHERE code = ' + mysql.escape(req.params.code) + ' AND ' + appType;

        pool.getConnection(function(err, connection) {
            if (err) {
                log.error('database error: ' + err);
                connection.release();
                res.status(500);
                return;
            }
            connection.query(query, function(err, rows) {
                connection.release();
                if (!err) {
                    let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                    log.info('sending binary file to: ' + ip + ' for code: ' + req.params.code + ' on OS: ' + req.params.os);
                    if (rows.affectedRows != 0) {
                        for (var key in info) {
                            if (info[key].os == req.params.os) {
                                var file = './files/' + info[key].os + '/' + info[key].build + '/' + info[key].localfile;
                                res.download(file);
                            }
                        }
                    } else {
                        res.status(401);
                        res.json("code not found");
                    }
                    return;
                }
            });
            connection.on('error', function(err) {
                log.error('database error: ' + err);
                res.status(500);
                return;
            });
        });
    })

/**
 * GET - /download/:build/:code/:os - increment download count for given code
 * @param - code - code to be incremented
 * @param - os - os download being requested
 **/
router.route('/download/:build/:code/:os')
    .get(function(req, res) {
        let appType = "beta";
        let info = INFO;

        switch (req.params.build) {
            case 'beta':
                appType = "(type is NULL OR type = 'beta')";
                info = INFO;
                break;
            case 'scif':
                appType = "type = 'scif'";
                info = INFO_SCIF;
                break;
            case 'pro':
                appType = "type = 'pro'";
                info = INFO_PRO;
                break;
            default:
                return res.status(400).end();
        }

        let query = 'UPDATE download_codes SET count = count + 1 WHERE code = ' + mysql.escape(req.params.code) + ' AND ' + appType;

        pool.getConnection(function(err, connection) {
            if (err) {
                log.error('database error: ' + err);
                connection.release();
                res.status(500);
                return;
            }
            connection.query(query, function(err, rows) {
                connection.release();
                if (!err) {
                    let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                    log.info('download request from: ' + ip + ' for code: ' + req.params.code + ' on OS: ' + req.params.os);
                    if (rows.affectedRows != 0) {
                        let osMatch = false;
                        for (var key in info) {
                            if (info[key].os == req.params.os) {
                                osMatch = true;
                                res.json({
                                    link: info[key].link,
                                    target: info[key].target
                                });
                            }
                        }
                        if (osMatch == false) {
                            res.status(404);
                            res.json("link unavailable");
                        }
                    } else {
                        res.status(401);
                        res.json("code not found");
                    }
                    return;
                }
            });
            connection.on('error', function(err) {
                log.error('database error: ' + err);
                res.status(500);
                return;
            });
        });
    })

/**
 * GET - /salt - generate random salt
 **/
router.route('/salt')
    .get(function(req, res) {
        var salt = bcrypt.genSaltSync(10);
        res.json(salt);
    })

/**
 * PUT /createCode -- generate and return a new code.
 * the following is required in the request body:
 * @param - user - username
 * @param - password - password
 * @param - max_count - maximum download count for new code
 **/
router.route('/createCode')
    .put(function(req, res) {
        let query = "SELECT password FROM user WHERE name = " + mysql.escape(req.body.user);

        pool.getConnection(function(err, connection) {
            if (err) {
                log.error('database error: ' + err);
                connection.release();
                res.status(500);
                return;
            }
            connection.query(query, function(err, rows) {
                connection.release();
                if (!err) {
                    let hash = bcrypt.hashSync(req.body.password, config.app.salt);

                    if (rows.length == 0) {
                        log.error('invalid login attempt, user: ' + req.body.user);
                        res.status(401);
                        res.json('invalid credentials');
                        return;
                    }

                    if (rows[0].password == hash) {

                        let newCode = randomstring.generate(8);

                        let query = "INSERT INTO download_codes (code, max_count, type) VALUES ('" + newCode + "', '" + mysql.escape(req.body.max_count) + "', " + mysql.escape(req.body.version) + ")";

                        console.log(query);

                        pool.getConnection(function(err, connection) {
                            if (err) {
                                log.error('database error: ' + err);
                                connection.release();
                                res.status(500);
                                return;
                            }
                            connection.query(query, function(err, rows) {
                                connection.release();
                                if (!err) {
                                    if (rows.affectedRows != 0) {
                                        let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                                        log.info('new code created by: ' + ip + '; code: ' + newCode);
                                        res.json(newCode);
                                    } else {
                                        res.status(500);
                                        res.json("error creating code");
                                    }
                                    return;
                                }
                            });
                            connection.on('error', function(err) {
                                log.error('database error: ' + err);
                                res.status(500);
                                return;
                            });
                        });


                    }
                    return;
                } else {
                    log.error('invalid login attempt, user: ' + req.body.user);
                    res.status(401);
                    res.json('invalid credentials');
                }
            });
            connection.on('error', function(err) {
                log.error('database error: ' + err);
                res.status(500);
                return;
            });
        });
    })

/**
 * POST /updateCode -- update the max count for a given code
 * the following is required in the request body:
 * @param - user - username
 * @param - password - password
 * @param - code - code to be updated
 * @param - max_count - new max count to be set
 **/
router.route('/updateCode')
    .post(function(req, res) {
        let query = "SELECT password FROM user WHERE name = " + mysql.escape(req.body.user);

        pool.getConnection(function(err, connection) {
            if (err) {
                log.error('database error: ' + err);
                connection.release();
                res.status(500);
                return;
            }
            connection.query(query, function(err, rows) {
                connection.release();
                if (!err) {
                    let hash = bcrypt.hashSync(req.body.password, config.app.salt);

                    if (rows.length == 0) {
                        log.error('invalid login attempt, user: ' + req.body.user);
                        res.status(401);
                        res.json('invalid credentials');
                        return;
                    }

                    if (rows[0].password == hash) {

                        let query = "UPDATE download_codes SET max_count='" + mysql.escape(req.body.max_count) + "' WHERE code=" + mysql.escape(req.body.code);

                        pool.getConnection(function(err, connection) {
                            if (err) {
                                log.error('database error: ' + err);
                                connection.release();
                                res.status(500);
                                return;
                            }
                            connection.query(query, function(err, rows) {
                                connection.release();
                                if (!err) {
                                    let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                                    log.info('code count updated by: ' + ip + '; code: ' + req.body.code);
                                    if (rows.affectedRows != 0) {
                                        res.json("success");
                                    } else {
                                        res.status(500);
                                        res.json("error updating code");
                                    }
                                    return;
                                }
                            });
                            connection.on('error', function(err) {
                                log.error('database error: ' + err);
                                res.status(500);
                                return;
                            });
                        });


                    }
                    return;
                } else {
                    log.error('invalid login attempt, user: ' + req.body.user);
                    res.status(401);
                    res.json('invalid credentials');
                }
            });
            connection.on('error', function(err) {
                log.error('database error: ' + err);
                res.status(500);
                return;
            });
        });
    })

/**
 * PUT /createUser -- generate a new user that can update codes
 * the following is required in the request body:
 * @param - user - current user
 * @param - password - password
 * @param - newUser - new user to be created
 * @param - newPassword - new password for new user
 **/
router.route('/createUser')
    .put(function(req, res) {
        let query = "SELECT password FROM user WHERE name = " + mysql.escape(req.body.user);

        pool.getConnection(function(err, connection) {
            if (err) {
                log.error('database error: ' + err);
                connection.release();
                res.status(500);
                return;
            }
            connection.query(query, function(err, rows) {
                connection.release();
                if (!err) {
                    let hash = bcrypt.hashSync(req.body.password, config.app.salt);

                    if (rows.length == 0) {
                        log.error('invalid login attempt, user: ' + req.body.user);
                        res.status(401);
                        res.json('invalid credentials');
                        return;
                    }

                    if (rows[0].password == hash) {

                        let newPasswordHash = bcrypt.hashSync(req.body.newPassword, config.app.salt)

                        let query = "INSERT INTO user (name, password) VALUES (" + mysql.escape(req.body.newUser) + ", '" + newPasswordHash + "')";

                        pool.getConnection(function(err, connection) {
                            if (err) {
                                log.error('database error: ' + err);
                                connection.release();
                                res.status(500);
                                return;
                            }
                            connection.query(query, function(err, rows) {
                                connection.release();
                                if (!err) {
                                    if (rows.affectedRows != 0) {
                                        let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                                        log.info('new user created by: ' + req.body.user + ' at ' + ip + '; username: ' + req.body.newUser);
                                        res.json("success");
                                    } else {
                                        res.status(500);
                                        res.json("error creating user");
                                    }
                                    return;
                                } else {
                                    log.error('unable to create new user: ' + req.body.newUser);
                                    res.status(500);
                                    res.json('unable to create new user');
                                }
                            });
                            connection.on('error', function(err) {
                                log.error('database error: ' + err);
                                res.status(500);
                                return;
                            });
                        });


                    }
                    return;
                } else {
                    log.error('invalid login attempt, user: ' + req.body.user);
                    res.status(401);
                    res.json('invalid credentials');
                }
            });
            connection.on('error', function(err) {
                log.error('database error: ' + err);
                res.status(500);
                return;
            });
        });
    })

/**
 * POST /getAllCodes -- get all codes in database
 * the following is required in the request body:
 * @param - user - current user
 * @param - password - password
 **/
router.route('/getAllCodes')
    .post(function(req, res) {
        let query = "SELECT password FROM user WHERE name = " + mysql.escape(req.body.user);

        pool.getConnection(function(err, connection) {
            if (err) {
                log.error('database error: ' + err);
                connection.release();
                res.status(500);
                return;
            }
            connection.query(query, function(err, rows) {
                connection.release();
                if (!err) {
                    let hash = bcrypt.hashSync(req.body.password, config.app.salt);

                    if (rows.length == 0) {
                        log.error('invalid login attempt, user: ' + req.body.user);
                        res.status(401);
                        res.json('invalid credentials');
                        return;
                    }

                    if (rows[0].password == hash) {

                        let query = 'SELECT * from download_codes';

                        pool.getConnection(function(err, connection) {
                            if (err) {
                                log.error('database error: ' + err);
                                connection.release();
                                res.status(500);
                                return;
                            }
                            connection.query(query, function(err, rows) {
                                connection.release();
                                if (!err) {
                                    res.json(rows);
                                    return;
                                } else {
                                    res.status(500);
                                    res.json('unknown error');
                                }
                            });
                            connection.on('error', function(err) {
                                log.error('database error: ' + err);
                                res.status(500);
                                return;
                            });
                        });
                    }
                    return;
                } else {
                    log.error('invalid login attempt, user: ' + req.body.user);
                    res.status(401);
                    res.json('invalid credentials');
                }
            });
            connection.on('error', function(err) {
                log.error('database error: ' + err);
                res.status(500);
                return;
            });
        });
    })

app.use('/api', router);

app.listen(config.app.port);
log.info('Beta Download Service running on port: ' + config.app.port);
