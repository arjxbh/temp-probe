const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const config = require('config');
const Log = require('log');
const log = new Log('debug');
const Q = require('q');
const bcrypt = require('bcrypt');
const randomstring = require("randomstring");
const fs = require('fs');
const exec = require('child_process').exec;
const os = require('os');

//TODO: find out -- what does this to??
require('events').EventEmitter.prototype._maxListeners = 100;

log.info('Starting temperature service at: ' + config.port);

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

var router = express.Router();

router.route('/getAllData')
  .get(function(req, res) {
    var fs = require('fs');
    //var dirname = '/Users/MyHome/git/temp-probe/sample_data';
    var dirname = '/home/arjun/git/temp-probe/sample_data';
    var results = [];

    console.log('getting all data from request');


    // function fs.readdir(input, callback){
    //     files = read_files_in_directory(input);
    //     if(!files){
    //       error = "something bad happened";
    //     }
    //     callback(error, files);
    // }

    fs.readdir(dirname, function(err, files) {
      if (err) {
        console.error("Could not list directory", err);
        //process.exit( 1 );
      }

      // console.log(files);
      var temperatureArray = [];
      var numFiles = files.length;
      var fileCount = 0;

      files.forEach(function(file, index) {
        // console.log("file at index[" + index + "]: = " + file);

        //console.log('loop is at index: ' + index + ' filename: ' + file);

        fs.readFile(dirname + '/' + file, 'utf8', function(err, data) {
          if (err) {
            return console.log(err);
          }
          //console.log('callback is at file: ' + file);
          // console.log(data);
          var dataArr = data.split("|");
          var celsius = parseFloat(dataArr[0]);
          var FahrenC = parseFloat((celsius * (9 / 5) + 32).toFixed(2));
          var humidity = parseFloat(dataArr[1]);
          var readtime = file;
          var timeC = new Date(readtime * 1000);

          temperatureArray.push({
            "temperature": FahrenC,
            "humidity": humidity,
            "time": timeC,
            "timestamp": parseInt(readtime)
          });
          fileCount++;
          if (fileCount == files.length) {
            res.json(temperatureArray);
          }
          //console.log('the temperature is ' + FahrenC + '*F  the humidity is ' + humidity);
        });
        //results.push({Temp: Time: });
      });
    });

    //console.log(files); // THIS IS AN ERROR!!
    //PART 1 make aync issue go away
    //PART 2 convert time to human readable time and then this func should be mostly done.
    //res.json('done');
  });


//function timeConverter(UNIX_timestamp){
// var a = new Date(UNIX_timestamp * 1000);
// var date = a.getDate();
// var hour = a.getHours();
// var min = a.getMinutes();
// var sec = a.getSeconds();
// var time = date + ' ' + hour + ':' + min + ':' + sec ;
// return time;
//   })

// router.route('/consumeAllData')
//   .get(function(req, res) {
//
//   });
//
// router.route('/consumeDataForPeriod/:start/:end')
//   .get(function(req, res) {
//
//   });
//
// route.route('/healthCheck')
//   .get(function(req, res) {
//
//   });
//
// route.route('/discover')
//   .get(function(req, res) {
//
//   });

app.use('/api', router);

app.listen(config.port);
log.info('Tempperature service running on port: ' + config.port);
