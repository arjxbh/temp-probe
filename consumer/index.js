const config = require('config');
const Log = require('log');
const log = new Log('debug');
const Q = require('q');
const fs = require('fs');
const exec = require('child_process').exec;
const os = require('os');
const http = require('http');

const sensors = [{
  "ip": "127.0.0.1",
  "port": 3703,
  "key": "dsfsdfsfdsfds"
}];

for (var index in sensors) {

  var options = {
    host: sensors[index].ip,
    port: sensors[index].port,
    path: '/api/getAllData',
    method: 'GET'
  }

  console.log(options);

  http.request(options, function(res) {
    console.log('STATUS: ' + res.statusCode);
    console.log('HEADERS: ' + JSON.stringify(res.headers));
    res.setEncoding('utf8');
    res.on('data', function(chunk) {
      console.log('BODY: ' + chunk);
    });
  }).end();
}
